FROM alpine:latest

COPY . .

ARG ADB_PHONE_IP
RUN apk update
RUN apk --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ add android-tools
RUN apk add jq
RUN apk add bash
RUN apk add ffmpeg
RUN apk add nodejs
RUN apk add npm
RUN apk add chromium

RUN cd Text && npm install
RUN find . -name "*.sh" -exec chmod +x {} \;
RUN chmod +x Tools/yt-dlp


CMD [ "./generate.sh" ]

