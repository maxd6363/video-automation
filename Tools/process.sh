#!/bin/bash

# This script uses the process.sh and speed-up.sh scripts to split a video into multiple parts and speed them up

# Check if the user has provided a video url
if [ -z "$1" ]
then
    echo "Please provide a video url"
    exit 1
fi

# Check if the user has provided a music url
if [ -z "$2" ]
then
    echo "Please provide a music url"
    exit 1
fi

# Check if the user has provided a split time

if [ -z "$3" ]
then
    echo "Please provide a split time"
    exit 1
fi

# Check if the user has provided a output file name
if [ -z "$4" ]
then
    echo "Please provide a output file name"
    exit 1
fi

# We check if the user has provided a json file
if [ -z "$5" ]
then
    echo "Please provide a json file"
    exit 1
fi

export YT_DLP_PATH=./yt-dlp

# We define a function to check if last command was successful
function check_error {
    if [ $? -ne 0 ]
    then
        echo "Error"
        exit 1
    fi
}

# We download the video
echo "--- Downloading video ---"
./download-video.sh "$1" "$4"
check_error

# We download the music
echo "--- Downloading music ---"
./download-audio.sh "$2" "$4"
check_error


# We merge the video and music
echo "--- Merging video and music ---"
./merge.sh "$4.mp4" "$4.mp3"
check_error


# We speed up the video
echo "--- Speeding up video ---"
./speed-up.sh "$4_merged.mp4"
check_error

# We split the video
echo "--- Splitting video ---"
./split.sh "$4_merged_speed_up.mp4" "$3" "$4"
check_error

# We delete the temporary files
echo "--- Deleting temporary files ---"
rm "$4.mp4"
rm "$4.mp3"
rm "$4_merged.mp4"
rm "$4_merged_speed_up.mp4"


# We copy the json file
echo "--- Copying json file ---"
cp "$5" "../Text/src/prompts.json"

# We run the process script
echo "--- Running process script ---"
cd ../Text
npm run bulk
cd ../Tools

# We move the generated files
echo "--- Moving generated files ---"
mkdir overlays
mv ../Text/out/* overlays

# We store all files in $4 folder in a array
backgrounds=("$4"/*)
overlays=(overlays/*)

# We merge the overlays and backgrounds
echo "--- Merging overlays and backgrounds ---"
mkdir output > /dev/null 2>&1

# we loop through the min length of the two arrays
if [ ${#backgrounds[@]} -lt ${#overlays[@]} ] 
then
    echo "[Warning] - Ignoring some overlays because there are not enough backgrounds [backgrounds=${#backgrounds[@]} overlays=${#overlays[@]}]]"
    min_length=${#backgrounds[@]}
else 
    min_length=${#overlays[@]}
fi

for (( i=0; i<$min_length; i++ ))
do
    ./merge-overlay.sh "${backgrounds[$i]}" "${overlays[$i]}" "output/$(basename "${overlays[$i]}" ".mkv")"
    check_error
done




