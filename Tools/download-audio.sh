#!/bin/bash

# This script downloads an audio from a url using yt-dlp

# Check if the YT_DLP_PATH is defined

if [ -z "$YT_DLP_PATH" ]
then
    echo "Please specify the path to the yt-dlp executable using YT_DLP_PATH variable"
    exit 1
fi

# Check if the user has provided a url

if [ -z "$1" ]
then
    echo "Please provide a url"
    exit 1
fi

# Check if the user has provided a output file name

if [ -z "$2" ]
then
    echo "Please provide a output file name"
    exit 1
fi

# We download the audio in mp3 format
$YT_DLP_PATH --extract-audio --audio-format mp3 -o "$2.mp3" "$1"
