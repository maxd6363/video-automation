#!/bin/bash

# This script splits a video into multiple parts using ffmpeg

# Check if the user has provided a video file
if [ -z "$1" ]
then
    echo "Please provide a video file"
    exit 1
fi

# Check if the user has provided a split time
if [ -z "$2" ]
then
    echo "Please provide a split time"
    exit 1
fi

# Check if the user has provided a output file name
if [ -z "$3" ]
then
    echo "Please provide a output file name"
    exit 1
fi


resolution=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 "$1")

if [ "$resolution" != "1920x1080" ]
then
    echo "The video is not in 1920x1080 resolution -> resizing"
    ffmpeg -hwaccel auto -i "$1" -vf scale=1920:1080 "${1%.*}_resized.mp4"
    mv "${1%.*}_resized.mp4" "$1"
fi

# We create a directory to store the split videos with the name of the video file plus _splitted
mkdir "$3"

# We crop the video to portrait mode
ffmpeg -hwaccel auto -i "$1" -vf "crop=607:1080:656:0" -c:a copy "${1%.*}_cropped.mp4"

# We change the resolution of the video to 1080x1920
ffmpeg -hwaccel auto -i "${1%.*}_cropped.mp4" -vf scale=1080:1920 "${1%.*}_cropped_1080p.mp4"

# We split the video into multiple parts
ffmpeg -hwaccel auto -i "${1%.*}_cropped_1080p.mp4" -c copy -map 0 -segment_time "$2" -f segment -reset_timestamps 1 "$3"/%03d.mp4

# We remove the cropped video
rm "${1%.*}_cropped.mp4"
rm "${1%.*}_cropped_1080p.mp4"
