#!/bin/bash

# This script merge audio and video files using ffmpeg

# Check if the user has provided a video file
if [ -z "$1" ]
then
    echo "Please provide a video file"
    exit 1
fi

# Check if the user has provided a audio file
if [ -z "$2" ]
then
    echo "Please provide an overlay file"
    exit 1
fi

# Check if the user has provided a output file name
if [ -z "$3" ]
then
    echo "Please provide a output file name"
    exit 1
fi

# We merge video and overlay
ffmpeg -hwaccel auto -i "$1" -i "$2" -filter_complex "[1]format=rgb24,colorkey=black[1d];[0][1d]overlay" -c:a copy "$3.mp4"

