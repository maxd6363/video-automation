#!/bin/bash

# This script speeds up a video using ffmpeg

# Check if the user has provided a video file

if [ -z "$1" ]
then
    echo "Please provide a video file"
    exit 1
fi

# We speed up the video and audio using ffmpeg

ffmpeg -hwaccel auto -i "$1" -filter:v "setpts=0.75*PTS" -filter:a "atempo=1.25" "${1%.*}_speed_up.mp4"
