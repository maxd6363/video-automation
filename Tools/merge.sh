#!/bin/bash

# This script merge audio and video files using ffmpeg

# Check if the user has provided a video file
if [ -z "$1" ]
then
    echo "Please provide a video file"
    exit 1
fi

# Check if the user has provided a audio file
if [ -z "$2" ]
then
    echo "Please provide a audio file"
    exit 1
fi

# We merge the audio and video files
ffmpeg -hwaccel auto -i "$1" -stream_loop -1 -i "$2" -c:v copy -shortest -c:a aac -map 0:v:0 -map 1:a:0 "${1%.*}_merged.mp4"