#!/bin/bash

if [ ! -f Data/urls.json ]
then
    echo "Data/urls.json does not exist"
    exit 1
fi

if [ ! -f Data/prompts.json ]
then
    echo "Data/prompts.json does not exist"
    exit 1
fi

if [ "$1" = "-g" ]
then
    # We generate the videos 

    urls=$(jq '.[0]' Data/urls.json)
    video_url=$(echo "$urls" | jq -r '.video')
    audio_url=$(echo "$urls" | jq -r '.audio')

    cd Tools
    ./process.sh $video_url $audio_url 15 test ../Data/prompts.json
    cd ..
fi

if [ "$1" = "-u" ]
then
    # We upload the videos

    if [ -z ${ADB_PHONE_IP+x} ]
    then
        echo "ADB_PHONE_IP is unset"
        exit 1
    fi

    adb connect ${ADB_PHONE_IP}

    # We check if we are connected to the phone
    if [ "$(adb devices | grep ${ADB_PHONE_IP})" == "" ]
    then
        echo "Could not connect to the phone"
        #exit 1
    fi

    videos=(Tools/output/*.mp4)
    echo "--- Uploading ${#videos[@]} videos ---" 

    i=0
    for video in "${videos[@]}"
    do
        title=$(basename "$video")
        echo "Uploading $title"
        ./Upload/upload.sh "$video" "$title"
        echo "Uploaded $title result : $?"
        i=$((i+1))
    done
    
    # rm Tools/output/*.mp4
    # rm Tools/test/*.mp4

fi






