#!/bin/bash

deviceInput="Upload/input-pad.json"


if [ -z "$1" ]
then
    echo "Please provide video path"
    exit 1
fi

if [ -z "$2" ]
then
    echo "Please provide title"
    exit 1
fi


function device-connect {
    device=$(adb devices -l | grep -v List | cut -d ' ' -f 1)

    if [ -z "$device" ]
    then
        echo "No device connected"
        echo "Trying to connect to device"
        adb connect $ADB_PHONE_IP
    fi
}

function device-touch {
    device-connect
    adb shell input tap $(jq '.['$1'].x' $deviceInput) $(jq '.['$1'].y' $deviceInput)
}

function device-start-app {
    device-connect
    adb shell am start -n "$1"
}

function device-toggle-power {
    device-connect
    adb shell input keyevent 26
}

function device-back {
    device-connect
    adb shell input keyevent 4
}

function device-kill-app {
    device-connect
    adb shell am force-stop "$1"
}

function device-type {
    device-connect
    adb shell input text "$(echo "$1" | sed -f Upload/commands.sed)"    
}

function device-remove-file {
    device-connect
    adb shell rm "/sdcard/DCIM/Camera/$1"
}

function device-push-file {
    device-connect
    adb push "$1" "/sdcard/DCIM/Camera/$2"
}


device-connect
echo "Working with $(adb devices -l | grep -v List | cut -d ' ' -f 1)"

# We update the file date to make sure it is the last one
touch "$1"	

device-remove-file "VID_20230912_063545.mp4"
device-push-file "$1" "VID_20230912_063545.mp4"

adb shell am broadcast -a android.intent.action.MEDIA_SCANNER_SCAN_FILE -d file:///sdcard

device-toggle-power
sleep 2
device-kill-app com.google.android.youtube
sleep 2
device-start-app com.google.android.youtube/com.google.android.apps.youtube.app.WatchWhileActivity
sleep 5
device-touch 0 # plus button
sleep 2
device-touch 1 # short button
sleep 2
device-touch 2 # first video
sleep 2
device-touch 3 # next
sleep 2
device-touch 4 # next
sleep 3
device-touch 5 # title
sleep 2
device-type 6 # type title
sleep 2
device-back # close keyboard
sleep 2
device-touch 7 # post
sleep 20 # wait for upload
device-toggle-power