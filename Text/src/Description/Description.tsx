import { useCurrentFrame } from "remotion";
import "./Description.css"
import { getClosestWord, map } from "../utils";


export const Description = ({ description }) => {
  const frame = useCurrentFrame();
  const total = 450;
  description = ` ${description}`

  // We wait 1.5 sec before starting the animation and then we stop 0.5s before the end

  const letterPosition = map(frame, 20, total - 15, 0, description.length);
  const word = getClosestWord(description, letterPosition < 0 ? 0 : letterPosition);
  
  console.log("letterPosition", letterPosition, "word", word);
  

  return (
    <>
      <div className="typewriter center-screen">
        <h1>{word}</h1>
      </div>
    </>
  )
};