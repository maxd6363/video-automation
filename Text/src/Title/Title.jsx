import './Title.css'

export const Title = ({ title }) => {
  
  const fontSize = 200 / title.length;
  console.log("fontSize", fontSize);

  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      marginTop: '5em',
      textAlign: 'center',
    }}>
      <p className="glitch">
        {/* <span>{title}</span> */}
        {title}
        {/* <span>{title}</span> */}
      </p>
    </div>
  );
};