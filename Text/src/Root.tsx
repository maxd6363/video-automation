import { Composition } from 'remotion';
import { SimpleScene } from './SimpleScene/SimpleScene';


export const RemotionRoot: React.FC = () => {
	return (
		<>
			<Composition
				id="SimpleScene"
				component={SimpleScene}
				durationInFrames={450}
				fps={30}
				width={1080}
				height={1920}
				defaultProps={{
					transparent: true
				}}				
			/>
		</>
	);
};
