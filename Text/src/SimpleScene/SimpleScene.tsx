import { Description } from '../Description/Description';
import { Title } from './../Title/Title';
import prompts from '../prompts.json';
import {getInputProps} from 'remotion'

export const SimpleScene = () => {
  const promptFromRender = getInputProps();
  const prompt = Object.keys(promptFromRender).length === 0 ? prompts[0] : promptFromRender;
  
  return (
    <div style={{
      width: '100%',
    }}>
      <Title title={prompt.title} />
      <Description description={prompt.description} />
    </div>
  )

};  