import { getCompositions, renderMedia } from "@remotion/renderer";
import { bundle } from "@remotion/bundler";
import data from "./prompts.json";


const start = async () => {
  const bundleLocation = await bundle({
    entryPoint: "./src/index.ts",
  });
  
  const compositionId = "SimpleScene";
  const allCompositions = await getCompositions(bundleLocation);
  const composition = allCompositions.find((c) => c.id === compositionId);

  if (!composition) {
    throw new Error(`No composition with the ID ${compositionId} found.`);
  }

  for (const entry of data) {
    await renderMedia({
      composition,
      serveUrl: bundleLocation,
      codec: "prores",
      imageFormat: "png",
      pixelFormat: "yuva444p10le",
      outputLocation: `out/${entry.title}.mkv`,
      inputProps: entry,
      proResProfile: "4444",
    });
  }
};

start()
  .then(() => {
    console.log("Rendered all videos");
  })
  .catch((err) => {
    console.log("Error occurred:", err);
  });