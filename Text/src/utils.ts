export const map = (x: number, inMin: number, inMax: number, outMin: number, outMax: number): number => {
  return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

export const getClosestWord = (str: string, pos: number) => {
  // Perform type conversions.
  str = String(str);
  pos = Number(pos) >>> 0;

  // Search for the word's beginning and end.
  const left = str.slice(0, pos + 1).search(/\S+$/);
  const right = str.slice(pos).search(/\s/);

  // The last word in the string is a special case.
  if (right < 0) {
      return str.slice(left);
  }
  // Return the word, using the located bounds to extract it from the string.
  return str.slice(left, right + pos);
}